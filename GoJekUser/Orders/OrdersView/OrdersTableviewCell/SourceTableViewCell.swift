//
//  SourceTableViewCell.swift
//  GoJekUser
//
//  Created by Sudar on 06/07/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class SourceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var staticSourceLabel: UILabel!
    @IBOutlet weak var sourceView: UIView!
    @IBOutlet weak var centerStatusView: UIView!

    @IBOutlet weak var pickupAddrStaticLbl: UILabel!
    @IBOutlet weak var pickupAddrValueLbl: UILabel!
    @IBOutlet weak var pickupStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setDarkMode()
        
        staticSourceLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        staticSourceLabel.text = OrderConstant.source.localized
        
        sourceLabel.textColor = .lightGray
        sourceLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        
        
        pickupAddrStaticLbl.font = UIFont.setCustomFont(name: .medium, size: .x14)
        pickupAddrStaticLbl.text = "Pickup Address"
        
        pickupAddrValueLbl.textColor = .lightGray
        pickupAddrValueLbl.font = UIFont.setCustomFont(name: .medium, size: .x14)

    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setDarkMode(){
         self.contentView.backgroundColor = .boxColor
     }
    
}
